/*
 * @file NT_Led.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function declarations.
 */

#ifndef NT_Led_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Led_h

class Led_t
{
public:
	Led_t();
	bool Enable = FALSE;
	bool Value_Sp = FALSE;	
	void Configure(bool Startup_Sp);
	void Attach(int Pin);
	void Init();
	void Deinit();
	void Set(bool State);
	void Toggle();

private:
	int _Pin;	
	bool _Startup_Sp = FALSE;
	void _Write();
};

#endif