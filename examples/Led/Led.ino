/*
 * @file Led.ino
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Simple example of led utilisation.
 */

#include <NT_Led.h>

Led_t Led_1;

void setup()
{
  Led_1.Configure(FALSE);
  Led_1.Attach(13);
  Led_1.Init();
}

void loop()
{
  Led_1.Toggle();
  delay(1000);
  Led_1.Set(FALSE);
  delay(1000);
}
