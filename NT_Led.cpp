/*
 * @file NT_Led.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function definitions.
 */

#include "Arduino.h"
#include "NT_Led.h"

Led_t::Led_t()
{	
}

void Led_t::Configure(bool Startup_Sp)
{
	_Startup_Sp = Startup_Sp;
}

void Led_t::Attach(int Pin)
{
	_Pin = Pin;
	pinMode(_Pin, OUTPUT);
}

void Led_t::Init()
{
	Enable = TRUE;

	Value_Sp = _Startup_Sp;
	_Write();
}

void Led_t::Deinit()
{
	Value_Sp = _Startup_Sp;
	_Write();

	Enable = FALSE;
}

void Led_t::_Write()
{
	digitalWrite(_Pin, Value_Sp);
}

void Led_t::Set(bool State)
{
	if (Enable == TRUE)
	{
		Value_Sp = State;
	}
	else if (Enable == FALSE)
	{
		Value_Sp = _Startup_Sp;
	}

	_Write();
}

void Led_t::Toggle()
{
	if (Enable == TRUE)
	{
		if (Value_Sp == TRUE)
		{
			Value_Sp = FALSE;
		}
		else if (Value_Sp == FALSE)
		{
			Value_Sp = TRUE;
		}
		else
		{
			// Do nothing
		}
	}
	else if (Enable == FALSE)
	{
		Value_Sp = _Startup_Sp;
	}

	_Write();
}